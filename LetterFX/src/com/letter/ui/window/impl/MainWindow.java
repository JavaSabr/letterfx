package com.letter.ui.window.impl;

import javafx.beans.property.DoubleProperty;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import rlib.ui.page.UIPage;
import rlib.ui.util.FXUtils;
import rlib.ui.window.impl.AbstractUIWindow;
import rlib.util.array.Array;

import com.letter.ui.component.impl.PageListControl;

/**
 * Реализация главного окна.
 * 
 * @author Ronn
 */
public class MainWindow extends AbstractUIWindow {

	public MainWindow(Stage stage, Array<Class<? extends UIPage>> availablePages) {
		super(stage, availablePages);

		final Pane rootNode = getRootNode();

		final PageListControl pageListControl = new PageListControl(this);

		final DoubleProperty width = pageListControl.prefWidthProperty();
		width.bind(rootNode.widthProperty());

		final DoubleProperty height = pageListControl.prefHeightProperty();
		height.set(50);

		FXUtils.addToPane(pageListControl, rootNode);
	}
}
