package com.letter.ui.component.impl;

import static com.letter.main.Start.PAGES;
import javafx.beans.property.DoubleProperty;
import javafx.geometry.Insets;
import javafx.scene.layout.HBox;
import rlib.ui.page.UIPage;
import rlib.ui.util.FXUtils;
import rlib.ui.window.UIWindow;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;
import rlib.util.table.Table;
import rlib.util.table.TableFactory;

import com.letter.ui.page.impl.ClientUIPage;
import com.letter.ui.page.impl.EquipmentUIPage;
import com.letter.ui.page.impl.GradeUIPage;
import com.letter.ui.page.impl.ServiceUIPage;

/**
 * Реализация контрола со списком переключателей между страницами.
 * 
 * @author Ronn
 */
public class PageListControl extends HBox {

	public static final String PAGE_LIST_CONTROL_ID = "PageListControl";

	public static final int MIN_HEIGHT = 60;

	private static final Insets INSETS = new Insets(10, 5, 0, 10);

	private static final Table<Class<? extends UIPage>, String> PAGE_TEXTS = TableFactory.newObjectTable();

	static {
		PAGE_TEXTS.put(ClientUIPage.class, "Заказчик");
		PAGE_TEXTS.put(GradeUIPage.class, "Комплектация");
		PAGE_TEXTS.put(EquipmentUIPage.class, "Оборудование");
		PAGE_TEXTS.put(ServiceUIPage.class, "Услуги");
	}

	/** список контролов для перехода по страницам */
	private final Array<PageControl> pageControls;

	public PageListControl(final UIWindow window) {
		super(PAGES.size());

		setId(PAGE_LIST_CONTROL_ID);

		this.pageControls = ArrayFactory.newArray(PageControl.class);

		int i = 1;

		for(final Class<? extends UIPage> pageType : PAGES) {

			final PageControl pageControl = new PageControl(window, pageType, String.valueOf(i++), PAGE_TEXTS.get(pageType));
			pageControl.setMinWidth(180);

			final DoubleProperty height = pageControl.prefHeightProperty();
			height.bind(heightProperty());

			final DoubleProperty width = pageControl.prefWidthProperty();
			width.bind(widthProperty().subtract(PAGES.size()));

			FXUtils.addToPane(pageControl, this);
			HBox.setMargin(pageControl, INSETS);

			pageControls.add(pageControl);
		}

		setMinHeight(MIN_HEIGHT);
	}
}
