package com.letter.ui.component.impl;

import javafx.beans.property.DoubleProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import rlib.ui.page.UIPage;
import rlib.ui.util.FXUtils;
import rlib.ui.window.UIWindow;
import rlib.ui.window.event.impl.SwitchPageUIWindowEvent;
import rlib.ui.window.event.target.impl.SwitchPageEventTarget;

/**
 * Реализация контрола для перехода на страницу.
 * 
 * @author Ronn
 */
public class PageControl extends HBox {

	public static final int BUTTON_SIZE = 40;

	public static final Insets LABEL_OFFSET = new Insets(0, 0, 0, 5);

	public static final String ID_PAGE_TEXT_LABEL = "page_text_label";
	public static final String ID_PAGE_NUM_BUTTON = "page_num_button";
	public static final String ID_PAGE_NUM_BUTTON_SELECTED = "page_num_button_selected";

	/** тип страницы, за которой закреплен контрол */
	private final Class<? extends UIPage> pageType;

	/** оено приложения */
	private final UIWindow window;

	/** кнопка переключения */
	private final Button button;

	public PageControl(final UIWindow window, final Class<? extends UIPage> pageType, final String num, final String text) {
		super(2);

		this.pageType = pageType;
		this.window = window;

		button = new Button(num);
		button.setId(ID_PAGE_NUM_BUTTON);
		button.setAlignment(Pos.CENTER);
		button.setOnAction(event -> showPage());

		DoubleProperty height = button.prefHeightProperty();
		height.set(BUTTON_SIZE);

		final DoubleProperty width = button.prefWidthProperty();
		width.set(BUTTON_SIZE);

		final Label label = new Label(text);
		label.setId(ID_PAGE_TEXT_LABEL);
		label.setAlignment(Pos.CENTER);

		height = label.prefHeightProperty();
		height.set(BUTTON_SIZE);

		HBox.setMargin(label, LABEL_OFFSET);

		FXUtils.addToPane(button, this);
		FXUtils.addToPane(label, this);

		window.addEventHandler(SwitchPageUIWindowEvent.EVENT_TYPE, event -> processEvent((SwitchPageUIWindowEvent) event));
	}

	public Class<? extends UIPage> getPageType() {
		return pageType;
	}

	public UIWindow getWindow() {
		return window;
	}

	/**
	 * Отобразить страницу этого контрола.
	 */
	private void showPage() {
		final UIWindow window = getWindow();
		window.showPage(getPageType());
	}

	protected void processEvent(SwitchPageUIWindowEvent event) {

		SwitchPageEventTarget eventTarget = (SwitchPageEventTarget) event.getTarget();

		final UIPage newPage = eventTarget.getNewPage();

		if(newPage != null && newPage.getClass().equals(pageType)) {
			button.setId(ID_PAGE_NUM_BUTTON_SELECTED);
		} else {
			button.setId(ID_PAGE_NUM_BUTTON);
		}
	}

}
