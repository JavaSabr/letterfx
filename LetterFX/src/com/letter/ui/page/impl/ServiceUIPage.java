package com.letter.ui.page.impl;

import javafx.scene.layout.Pane;
import rlib.ui.page.impl.AbstractUIPage;
import rlib.ui.window.UIWindow;

/**
 * Реализацичя страницы конфигурации письма.
 * 
 * @author Ronn
 */
public class ServiceUIPage extends AbstractUIPage {

	public static final int FIELD_OFFSET = 50;
	public static final int FIELD_HEIGHT = 30;
	public static final int CLIENT_NANE_WIDTH = 130;

	@Override
	protected void initImpl(UIWindow window, Pane root) {
		super.initImpl(window, root);

	}
}
