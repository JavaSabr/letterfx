package com.letter.ui.page.impl;

import javafx.beans.property.DoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;
import jfxtras.scene.control.ListSpinner;

import org.controlsfx.control.textfield.TextFields;

import rlib.ui.page.impl.AbstractUIPage;
import rlib.ui.util.FXUtils;
import rlib.ui.window.UIWindow;

import com.letter.main.Start;

/**
 * Реализацичя страницы конфигурации письма.
 * 
 * @author Ronn
 */
public class ClientUIPage extends AbstractUIPage {

	public static final Insets INSETS_10 = new Insets(10);

	public static final int LABEL_WIDTH = 100;
	public static final int SPINNER_WIDTH = 100;

	/**
	 * Создание кнопки добавления.
	 */
	protected void createButtonAdd(final VBox column) {

		final HBox container = new HBox(1);
		container.setAlignment(Pos.CENTER);

		DoubleProperty width = container.prefWidthProperty();
		width.bind(column.widthProperty().subtract(LABEL_WIDTH));

		final Button button = new Button("Добавить вариант");

		width = button.prefWidthProperty();
		width.bind(container.widthProperty());

		FXUtils.addToPane(button, container);
		FXUtils.addToPane(container, column);
	}

	/**
	 * Создание поля для ввода имейла.
	 */
	protected void createClientEmail(final VBox column) {

		final HBox container = new HBox(2);

		DoubleProperty width = container.prefWidthProperty();
		width.bind(column.widthProperty());

		final Label label = new Label("Email:");
		label.setAlignment(Pos.CENTER_RIGHT);
		label.setPrefWidth(LABEL_WIDTH);
		label.setMinWidth(LABEL_WIDTH);

		final DoubleProperty height = label.prefHeightProperty();
		height.bind(container.heightProperty());

		final TextField field = TextFields.createClearableTextField();

		width = field.prefWidthProperty();
		width.bind(column.widthProperty().subtract(LABEL_WIDTH));

		FXUtils.addToPane(label, container);
		FXUtils.addToPane(field, container);
		FXUtils.addToPane(container, column);
	}

	/**
	 * Создание поля для вода имени клиента
	 */
	protected void createClientName(final VBox column) {

		final HBox container = new HBox(2);

		DoubleProperty width = container.prefWidthProperty();
		width.bind(column.widthProperty());

		final Label label = new Label("Клиент:");
		label.setAlignment(Pos.CENTER_RIGHT);
		label.setPrefWidth(LABEL_WIDTH);
		label.setMinWidth(LABEL_WIDTH);

		final DoubleProperty height = label.prefHeightProperty();
		height.bind(container.heightProperty());

		final TextField field = TextFields.createClearableTextField();

		width = field.prefWidthProperty();
		width.bind(column.widthProperty().subtract(LABEL_WIDTH));

		FXUtils.addToPane(label, container);
		FXUtils.addToPane(field, container);
		FXUtils.addToPane(container, column);
	}

	/**
	 * Создание поля для ввода номера телефона клиента.
	 */
	protected void createClientPhone(final VBox column) {

		final HBox container = new HBox(2);

		DoubleProperty width = container.prefWidthProperty();
		width.bind(column.widthProperty());

		final Label label = new Label("Телефон:");
		label.setAlignment(Pos.CENTER_RIGHT);
		label.setPrefWidth(LABEL_WIDTH);
		label.setMinWidth(LABEL_WIDTH);

		final DoubleProperty height = label.prefHeightProperty();
		height.bind(container.heightProperty());

		final TextField field = TextFields.createClearableTextField();

		width = field.prefWidthProperty();
		width.bind(column.widthProperty().subtract(LABEL_WIDTH));

		FXUtils.addToPane(label, container);
		FXUtils.addToPane(field, container);
		FXUtils.addToPane(container, column);
	}

	/**
	 * Создание колонки контактов с клиентом.
	 */
	protected void createContacts(final Pane root) {

		final VBox column = new VBox(3);

		final DoubleProperty width = column.prefWidthProperty();
		width.bind(root.widthProperty().subtract(2));

		createClientName(column);
		createClientPhone(column);
		createClientEmail(column);

		FXUtils.addToPane(column, root);

		HBox.setMargin(column, new Insets(30, 10, 10, 10));
	}

	/**
	 * Создания поля для указания даты скидки.
	 */
	protected void createDiscount(final VBox column) {

		final HBox container = new HBox(2);

		DoubleProperty width = container.prefWidthProperty();
		width.bind(column.widthProperty());

		final Label label = new Label("Скидка до:");
		label.setAlignment(Pos.CENTER_RIGHT);
		label.setPrefWidth(LABEL_WIDTH);
		label.setMinWidth(LABEL_WIDTH);

		final DoubleProperty height = label.prefHeightProperty();
		height.bind(container.heightProperty());

		final DatePicker datePicker = new DatePicker();
		datePicker.setEditable(false);

		width = datePicker.prefWidthProperty();
		width.bind(column.widthProperty().subtract(LABEL_WIDTH));

		FXUtils.addToPane(label, container);
		FXUtils.addToPane(datePicker, container);
		FXUtils.addToPane(container, column);
	}

	/**
	 * Создания поля для выбора типа фасада
	 */
	protected void createFacadeType(final VBox column) {

		final HBox container = new HBox(2);

		DoubleProperty width = container.prefWidthProperty();
		width.bind(column.widthProperty());

		final Label label = new Label("Фасад:");
		label.setAlignment(Pos.CENTER_RIGHT);
		label.setPrefWidth(LABEL_WIDTH);
		label.setMinWidth(LABEL_WIDTH);

		final DoubleProperty height = label.prefHeightProperty();
		height.bind(container.heightProperty());

		final ComboBox<Object> facadeBox = new ComboBox<>();
		facadeBox.getItems().add("фасад 1");
		facadeBox.setEditable(true);

		width = facadeBox.prefWidthProperty();
		width.bind(column.widthProperty().subtract(LABEL_WIDTH));

		FXUtils.addToPane(label, container);
		FXUtils.addToPane(facadeBox, container);
		FXUtils.addToPane(container, column);
	}

	/**
	 * Создание поля для указания стоимости и скидки.
	 */
	protected void createPrice(final VBox column) {

		final HBox container = new HBox(3);

		DoubleProperty width = container.prefWidthProperty();
		width.bind(column.widthProperty());

		final Label label = new Label("Цена:");
		label.setAlignment(Pos.CENTER_RIGHT);
		label.setPrefWidth(LABEL_WIDTH);
		label.setMinWidth(LABEL_WIDTH);

		final DoubleProperty height = label.prefHeightProperty();
		height.bind(container.heightProperty());

		final TextField field = TextFields.createClearableTextField();
		field.setTooltip(new Tooltip("Стоимость без скидки"));
		field.setPrefHeight(35);

		width = field.prefWidthProperty();
		width.bind(column.widthProperty().subtract(LABEL_WIDTH));

		final ObservableList<Integer> items = FXCollections.observableArrayList();

		for(int i = 0, length = 100; i <= length; i++) {
			items.add(i);
		}

		final ListSpinner<Integer> spinner = new ListSpinner<>(items);
		spinner.setEditable(true);
		spinner.setCyclic(true);
		spinner.setTooltip(new Tooltip("% скидки"));
		spinner.setMinWidth(SPINNER_WIDTH);
		spinner.setPrefWidth(SPINNER_WIDTH);
		spinner.setStringConverter(new StringConverter<Integer>() {

			@Override
			public Integer fromString(final String string) {
				return Integer.parseInt(string);
			}

			@Override
			public String toString(final Integer object) {
				return String.valueOf(object);
			}
		});

		spinner.prefHeightProperty().bind(field.heightProperty());

		FXUtils.addToPane(label, container);
		FXUtils.addToPane(field, container);
		FXUtils.addToPane(spinner, container);
		FXUtils.addToPane(container, column);
	}

	/**
	 * Создание колонки для заполнения продукта.
	 */
	protected void createProductInfo(final Pane root) {

		final VBox column = new VBox(4);
		column.setPrefHeight(100);
		column.setMaxHeight(130);

		final DoubleProperty width = column.prefWidthProperty();
		width.bind(root.widthProperty().subtract(2));

		createDiscount(column);
		createFacadeType(column);
		createPrice(column);
		createButtonAdd(column);

		FXUtils.addToPane(column, root);

		HBox.setMargin(column, new Insets(30, 10, 10, 10));
	}

	@Override
	protected Pane createRoot() {
		return new VBox(2);
	}

	@Override
	protected void initImpl(final UIWindow window, final Pane root) {
		super.initImpl(window, root);

		final HBox infoNode = new HBox(2);

		createContacts(infoNode);
		createProductInfo(infoNode);

		FXUtils.addToPane(infoNode, getRootNode());

		createClearLink();
	}

	@Override
	public void postPageShow(UIWindow window) {
		super.postPageShow(window);

		window.setSize(Start.WINDOW_WIDTH, Start.WINDOW_HEIGHT);
	}

	/**
	 * Создание линка для очистки формы.
	 */
	protected void createClearLink() {

		Hyperlink hyperlink = new Hyperlink("Очистить форму");

		FXUtils.addToPane(hyperlink, getRootNode());

		VBox.setMargin(hyperlink, INSETS_10);
	}
}
