package com.letter.ui.page.impl;

import java.util.Collections;
import java.util.List;

import javafx.beans.property.DoubleProperty;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import org.controlsfx.control.textfield.AutoCompletionBinding.ISuggestionRequest;
import org.controlsfx.control.textfield.TextFields;

import rlib.logging.Logger;
import rlib.logging.LoggerManager;
import rlib.ui.page.impl.AbstractUIPage;
import rlib.ui.util.FXUtils;
import rlib.ui.window.UIWindow;

/**
 * Реализацичя страницы конфигурации письма.
 * 
 * @author Ronn
 */
public class GradeUIPage extends AbstractUIPage {

	protected static final Logger LOGGER = LoggerManager.getLogger(GradeUIPage.class);

	public static final String BOTTOM_BAR_ID = "GradeUIPage_BottomBar";
	public static final String BUTTON_REVIEW_ID = "GradeUIPage_ButtonReview";
	public static final String BUTTON_ADD_ID = "GradeUIPage_ButtonAdd";
	public static final String BUTTON_NEXT_ID = "GradeUIPage_ButtonNext";

	private static final int LABEL_HEIGHT = 20;

	private static final int BUTTON_NEXT_HEIGHT = 30;
	private static final int BUTTON_NEXT_WIDTH = 140;

	private static final int BOTTOM_BAR_HEIGHT = 60;

	private static final int LABEL_WIDTH = 150;
	private static final int BUTTON_WIDTH = 200;

	private static final Insets COLUMN_INSETS = new Insets(20, 10, 10, 10);

	/** оснавная область страницы */
	private HBox infoNode;
	/** нижний тулбар страницы */
	private HBox bottomBarNode;

	protected void createBottomBar(final Pane root) {

		root.setMinHeight(BOTTOM_BAR_HEIGHT);
		root.setMaxHeight(BOTTOM_BAR_HEIGHT);

		final Button buttonNext = new Button("Далее   >");
		buttonNext.setId(BUTTON_NEXT_ID);
		buttonNext.setAlignment(Pos.CENTER);
		buttonNext.setPrefWidth(BUTTON_NEXT_WIDTH);
		buttonNext.setPrefHeight(BUTTON_NEXT_HEIGHT);

		Hyperlink clearForm = new Hyperlink("Очистить форму");

		final DoubleProperty width = clearForm.prefWidthProperty();
		width.bind(root.widthProperty().subtract(BUTTON_NEXT_WIDTH));

		FXUtils.addToPane(clearForm, root);
		FXUtils.addToPane(buttonNext, root);

		HBox.setMargin(buttonNext, new Insets(10));
		HBox.setMargin(clearForm, new Insets(10));
	}

	/**
	 * Создание колонки с кнопками добавления и просмотра.
	 */
	protected void createButtons(final Pane root) {

		final VBox column = new VBox(2);

		final DoubleProperty height = column.prefHeightProperty();
		height.bind(root.heightProperty());

		final DoubleProperty width = column.prefWidthProperty();
		width.set(BUTTON_WIDTH);

		final Button buttonAdd = new Button("Добавить");
		buttonAdd.setId(BUTTON_ADD_ID);
		buttonAdd.setPrefWidth(BUTTON_WIDTH);

		final Button buttonReview = new Button("Просмотр");
		buttonReview.setId(BUTTON_REVIEW_ID);
		buttonReview.setPrefWidth(BUTTON_WIDTH);

		FXUtils.addToPane(buttonAdd, column);
		FXUtils.addToPane(buttonReview, column);
		FXUtils.addToPane(column, root);

		VBox.setMargin(buttonAdd, new Insets(LABEL_HEIGHT, 10, 10, 10));
		VBox.setMargin(buttonReview, new Insets(0, 10, 10, 10));

		HBox.setMargin(column, COLUMN_INSETS);
	}

	/**
	 * Создание колонки с цветом.
	 */
	protected void createColor(final Pane root) {

		final VBox column = new VBox(3);

		final DoubleProperty height = column.prefHeightProperty();
		height.bind(root.heightProperty());

		final DoubleProperty width = column.prefWidthProperty();
		width.bind(root.widthProperty().subtract(BUTTON_WIDTH).divide(2));

		createColorField(column);
		createColorList(column);

		FXUtils.addToPane(column, root);

		HBox.setMargin(column, COLUMN_INSETS);
	}

	/**
	 * Создание поля для вода цвета комплектации
	 */
	protected void createColorField(final VBox column) {

		final VBox container = new VBox(2);

		DoubleProperty width = container.prefWidthProperty();
		width.bind(column.widthProperty());

		DoubleProperty height = container.prefHeightProperty();
		height.set(40);

		final Label label = new Label("Цвет, артикул");
		label.setAlignment(Pos.CENTER_LEFT);
		label.setPrefWidth(LABEL_WIDTH);
		label.setPrefHeight(LABEL_HEIGHT);

		height = label.prefHeightProperty();
		height.bind(container.heightProperty());

		final TextField field = TextFields.createClearableTextField();

		width = field.prefWidthProperty();
		width.bind(column.widthProperty().subtract(LABEL_WIDTH));

		TextFields.bindAutoCompletion(field, param -> findColors(param));

		FXUtils.addToPane(label, container);
		FXUtils.addToPane(field, container);
		FXUtils.addToPane(container, column);
	}

	/**
	 * Создание списка цветов комплектации
	 */
	protected void createColorList(final VBox column) {

		final ListView<String> titles = new ListView<>();

		titles.prefWidthProperty().bind(column.widthProperty());
		titles.prefHeightProperty().bind(column.heightProperty());

		final ObservableList<String> items = titles.getItems();
		items.addAll("цвет 1", "цвет 2", "цвет 3");

		FXUtils.addToPane(titles, column);
	}

	/**
	 * Создание колонки с наименованием.
	 */
	protected void createTitle(final Pane root) {

		final VBox column = new VBox(3);

		final DoubleProperty height = column.prefHeightProperty();
		height.bind(root.heightProperty());

		final DoubleProperty width = column.prefWidthProperty();
		width.bind(root.widthProperty().subtract(BUTTON_WIDTH).divide(2));

		createTitleField(column);
		createTitleList(column);

		FXUtils.addToPane(column, root);

		HBox.setMargin(column, COLUMN_INSETS);
	}

	/**
	 * Создание поля для вода названия комплектации
	 */
	protected void createTitleField(final VBox column) {

		final VBox container = new VBox(2);

		DoubleProperty width = container.prefWidthProperty();
		width.bind(column.widthProperty());

		DoubleProperty height = container.prefHeightProperty();
		height.set(40);

		final Label label = new Label("Наимнование");
		label.setAlignment(Pos.CENTER_LEFT);
		label.setPrefWidth(LABEL_WIDTH);
		label.setPrefHeight(LABEL_HEIGHT);

		height = label.prefHeightProperty();
		height.bind(container.heightProperty());

		final TextField field = TextFields.createClearableTextField();

		width = field.prefWidthProperty();
		width.bind(column.widthProperty().subtract(LABEL_WIDTH));

		TextFields.bindAutoCompletion(field, param -> findTitles(param));

		FXUtils.addToPane(label, container);
		FXUtils.addToPane(field, container);
		FXUtils.addToPane(container, column);
	}

	/**
	 * Создание списка названий комплектации
	 */
	protected void createTitleList(final VBox column) {

		final ListView<String> titles = new ListView<>();

		titles.prefWidthProperty().bind(column.widthProperty());
		titles.prefHeightProperty().bind(column.heightProperty());

		final ObservableList<String> items = titles.getItems();
		items.addAll("1", "2", "3");

		FXUtils.addToPane(titles, column);
	}

	protected List<String> findColors(final ISuggestionRequest param) {
		return Collections.emptyList();
	}

	protected List<String> findTitles(final ISuggestionRequest param) {
		return Collections.emptyList();
	}

	@Override
	protected void initImpl(final UIWindow window, final Pane root) {
		super.initImpl(window, root);

		infoNode = new HBox(3);

		createTitle(infoNode);
		createColor(infoNode);
		createButtons(infoNode);

		final DoubleProperty height = infoNode.prefHeightProperty();
		height.bind(root.heightProperty().subtract(BOTTOM_BAR_HEIGHT));

		DoubleProperty width = infoNode.prefWidthProperty();
		width.bind(root.widthProperty());

		bottomBarNode = new HBox(3);
		bottomBarNode.setId(BOTTOM_BAR_ID);
		bottomBarNode.setAlignment(Pos.CENTER_RIGHT);

		createBottomBar(bottomBarNode);

		width = bottomBarNode.prefWidthProperty();
		width.bind(root.widthProperty());

		FXUtils.addToPane(infoNode, getRootNode());
		FXUtils.addToPane(bottomBarNode, getRootNode());

		VBox.setMargin(bottomBarNode, new Insets(20, 0, 0, 0));
	}

	@Override
	public void postPageShow(final UIWindow window) {
		super.postPageShow(window);

		window.setSize(1000, 500);
	}
}
