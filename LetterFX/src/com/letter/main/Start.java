package com.letter.main;

import javafx.application.Application;
import javafx.stage.Stage;
import rlib.logging.LoggerLevel;
import rlib.ui.page.UIPage;
import rlib.ui.window.UIWindow;
import rlib.util.array.Array;
import rlib.util.array.ArrayFactory;

import com.letter.ui.page.impl.ClientUIPage;
import com.letter.ui.page.impl.EquipmentUIPage;
import com.letter.ui.page.impl.GradeUIPage;
import com.letter.ui.page.impl.ServiceUIPage;
import com.letter.ui.window.impl.MainWindow;

/**
 * Стартовый класс приложения.
 * 
 * @author Ronn
 */
public class Start extends Application {

	public static final int WINDOW_HEIGHT = 300;
	public static final int WINDOW_WIDTH = 800;

	public static final Array<Class<? extends UIPage>> PAGES = ArrayFactory.newArray(Class.class);

	static {
		PAGES.add(ClientUIPage.class);
		PAGES.add(GradeUIPage.class);
		PAGES.add(EquipmentUIPage.class);
		PAGES.add(ServiceUIPage.class);
	}

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage paramStage) throws Exception {

		Array<Class<? extends UIPage>> pages = ArrayFactory.newArray(Class.class);
		pages.addAll(PAGES);

		LoggerLevel.DEBUG.setEnabled(true);

		UIWindow window = new MainWindow(paramStage, pages);
		window.loadStylesheets("/style/style.css");
		window.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		window.setMinimalSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		window.setTitle("LetterFX by Ronn");
		window.moveToCenter();
		window.show();
	}
}
